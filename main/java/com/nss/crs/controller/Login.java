////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.controller;
////////////////////////////////////////////////////////////////////////////////
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
////////////////////////////////////////////////////////////////////////////////
public class Login extends HttpServlet {
    
    final private String LOGIN="NSS";
    final private String PASSWORD="NSS22";
    final private String ROLE="admin";
  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String userid=request.getParameter("login");
        String password=request.getParameter("password");

        if(userid.equals(LOGIN) && password.equals(PASSWORD)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", userid);
            session.setAttribute("role", ROLE);
            session.setMaxInactiveInterval(30*60);
            response.sendRedirect("CarInfo");
        } else {
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }  
}
////////////////////////////////////////////////////////////////////////////////