////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.controller.car;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.*;
import com.nss.crs.persistence.*;
import com.nss.crs.persistence.impl.*;
import com.nss.crs.persistence.util.DAOFactory;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
@WebServlet(name = "CarInfo", urlPatterns = {"/CarInfo"})
public class CarCRUD extends HttpServlet {
    
    private static String LIST = "/WEB-INF/views/car/list.jsp";
    private static String EDIT = "/WEB-INF/views/car/edit.jsp";
    private static String INSERT= "/WEB-INF/views/car/insert.jsp";
    
    private CarDAO cDao = DAOFactory.getDAOFactory().getCarDAO();
    private OrderDAO oDao = DAOFactory.getDAOFactory().getOrderDAO();
       
//------------------------------------------------------------------------------
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");
        if(action!=null) {
            if(action.equalsIgnoreCase("delete")) {
                Long id = Long.parseLong(request.getParameter("carId"));
                cDao.delete(cDao.findEntityByID(id));
                forward=LIST;
                request.setAttribute("listCar", cDao.findAll());
            } else if (action.equalsIgnoreCase("edit")) {
                forward = EDIT;
                Long id = Long.parseLong(request.getParameter("carId"));
                Car car=cDao.findEntityByID(id);
                request.setAttribute("car", car);
            } else if (action.equalsIgnoreCase("list")) {
                forward =LIST;
                request.setAttribute("listCar", cDao.findAll());
            } else {
                forward = INSERT;
            }
        }
        else {
            forward = LIST;
            request.setAttribute("listCar", cDao.findAll());
            
        }
        request.getRequestDispatcher(forward).forward(request, response);
    }
//------------------------------------------------------------------------------
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        
        Car car=new Car();
        String id = request.getParameter("id");
        String company = request.getParameter("company");
        String model = request.getParameter("model");
        String body = request.getParameter("body");
        String number = request.getParameter("number");
        String date = request.getParameter("date");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        car.setId(Long.parseLong(id));
        car.setCompanyManufacturer(company);
        car.setModel(model);
        car.setBodyType(body);
        car.setCarNumber(number);
        try {
            car.setDateConstruction(format.parse(date));
        } catch (ParseException ex) {
            Logger.getLogger(CarCRUD.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(action.equalsIgnoreCase("insert")) {
            cDao.insert(car);
        } else if(action.equalsIgnoreCase("edit")) {
            cDao.update(car);
        }
        request.setAttribute("listCar", cDao.findAll());
        request.getRequestDispatcher(LIST).forward(request, response);
    }
}
////////////////////////////////////////////////////////////////////////////////