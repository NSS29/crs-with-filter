////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.controller.filter;
////////////////////////////////////////////////////////////////////////////////
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class InsertationSecurity implements Filter {
    
    public void init(FilterConfig filterConfig) {}
    
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        
        HttpSession session = httpRequest.getSession(false);
        String role=(String)session.getAttribute("role");
        String action = request.getParameter("action");
        
        if(action.equals("insert")) {
           if(role.equals("guest")) {
                chain.doFilter(request, response);
            } else {
               httpResponse.sendRedirect(httpRequest.getContextPath()+"/CarInfo");
            }
        } else if(action.equals("edit")) {
            if(role.equals("guest")){
                chain.doFilter(request, response);
            } else {
                httpResponse.sendRedirect(httpRequest.getContextPath()+"/CarInfo");
            }
        }      
    }
    
    public void destroy() {}
}
////////////////////////////////////////////////////////////////////////////////