////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.controller.filter;
////////////////////////////////////////////////////////////////////////////////
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class PageRedirectSecurity implements Filter {
    
    private String loginPath; 
    
    public void init(FilterConfig filterConfig) {        
        loginPath=filterConfig.getInitParameter("LOGIN_PATH");
        
    }
    
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        
        HttpSession session = httpRequest.getSession(false);
        
        if(session != null) {
            String user=(String)session.getAttribute("user");
            if(user != null) {
                if(user.equals("NSS")) {
                    chain.doFilter(request, response);
                } else {
                    httpResponse.sendRedirect(httpRequest.getContextPath()+loginPath);
                }                
            } else {
                httpResponse.sendRedirect(httpRequest.getContextPath()+loginPath);
            }
        }else {
            httpResponse.sendRedirect(httpRequest.getContextPath()+loginPath);
        }  
    }
    
    public void destroy() {}
}
////////////////////////////////////////////////////////////////////////////////