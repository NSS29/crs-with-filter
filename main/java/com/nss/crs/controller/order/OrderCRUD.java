////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.controller.order;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.*;
import com.nss.crs.persistence.*;
import com.nss.crs.persistence.impl.*;
import com.nss.crs.persistence.util.DAOFactory;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
@WebServlet(name = "OrderInfo", urlPatterns = {"/OrderInfo"})
public class OrderCRUD extends HttpServlet {
    
    private Long carId;
    
    private CarDAO cDao = DAOFactory.getDAOFactory().getCarDAO();
    private OrderDAO oDao = DAOFactory.getDAOFactory().getOrderDAO();
    
//------------------------------------------------------------------------------
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String action = request.getParameter("action");
        if(action.equalsIgnoreCase("form")) 
            carId=Long.parseLong(request.getParameter("carId"));
        request.getRequestDispatcher("/WEB-INF/views/order/form.jsp").forward(request, response);
    }
//------------------------------------------------------------------------------
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Order order=new Order();
        String id = request.getParameter("id");
        String fDate = request.getParameter("firstDate");
	String lDate = request.getParameter("lastDate");
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        
        order.setId(Long.parseLong(id));
        try {
            order.setFirstRentalDay(format.parse(fDate));
            order.setLastRentalDay(format.parse(lDate));
        } catch (ParseException ex) {
            Logger.getLogger(OrderCRUD.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        oDao.insert(order);
        cDao.findEntityByID(carId).addOrder(order);
        
        request.setAttribute("order", order);
        request.getRequestDispatcher("/WEB-INF/views/car/order.jsp").forward(request, response);
    }
}
////////////////////////////////////////////////////////////////////////////////