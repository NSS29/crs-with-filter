////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.model;
////////////////////////////////////////////////////////////////////////////////
import java.io.*;
import java.util.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
public class Car extends Entity implements Serializable, Comparable {
    private Long id;
    private String companyManufacturer;
    private String model;
    private String bodyType;
    private String carNumber;
    private Date dateConstruction;
    private Boolean available;
    
    private Set<Order> setOrder=new LinkedHashSet();    
    
    public Car() {}
    
    public Car(Long id, String companyManufacturer, String model, String bodyType,
            String carNumber) {
        super(id);
        this.companyManufacturer=companyManufacturer;
        this.model=model;
        this.bodyType=bodyType;
        this.carNumber=carNumber;
    }
    public Car(Long id, String companyManufacturer, String model, String bodyType,
            String carNumber, Date dateConstruction) {
        this(id, companyManufacturer, model, bodyType, carNumber);
        this.dateConstruction=dateConstruction;
    }

    public String getCompanyManufacturer() {
        return companyManufacturer;
    }
    public void setCompanyManufacturer(String companyManufacturer) {
        if(companyManufacturer==null)
            throw new IllegalArgumentException();
        this.companyManufacturer = companyManufacturer;
    }
    
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        if(model==null)
            throw new IllegalArgumentException();
        this.model = model;
    }
    
    public String getBodyType() {
        return bodyType;
    }
    public void setBodyType(String bodyType) {
        if(bodyType==null)
            throw new IllegalArgumentException();
        this.bodyType = bodyType;
    }

    public String getCarNumber() {
        return carNumber;
    }
    public void setCarNumber(String carNumber) {
        if(carNumber==null && carNumber.length()<=8)
            throw new IllegalArgumentException();
        this.carNumber = carNumber;
    }
    
    public Boolean getAvailable() {
        return available;
    }
    public void setAvailable(Boolean available) {
        if(available==null)
            throw new IllegalArgumentException();
        this.available = available;
    }
    
    public Date getDateConstruction() {
        return dateConstruction;
    }
    public void setDateConstruction(Date dateConstruction) {
        if(dateConstruction==null)
            throw new IllegalArgumentException();
        this.dateConstruction = dateConstruction;
    }
    
    public Set<Order> getSetOrder() {
        return setOrder;
    }
    public void setSetOrder(Set<Order> setOrder) {
        if(setOrder==null)
            throw new IllegalArgumentException();
        this.setOrder = setOrder;
    }
    public void addOrder(Order order) {
        order.setCar(this);
        getSetOrder().add(order);
    }
    public void remove(Order order) {
        getSetOrder().remove(order);
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj==this)
            return true;
        if(obj==null)
            return false;
        if(getClass()!=obj.getClass())
            return false;
        Car c=(Car) obj;
        return (
                getId()==c.getId() 
                && getCompanyManufacturer().equalsIgnoreCase(c.getCompanyManufacturer())
                && getModel().equalsIgnoreCase(c.getModel())
                && getBodyType().equalsIgnoreCase(c.getBodyType())
                && getCarNumber().equalsIgnoreCase(c.getCarNumber())
                && getDateConstruction().equals(c.getDateConstruction()));
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 13;
        result = PRIME * result + getId().hashCode()+getCompanyManufacturer().hashCode()
                +getModel().hashCode()+getBodyType().hashCode()
                +getCarNumber().hashCode()+getDateConstruction().hashCode();
        return result;
    }
    
    @Override
    public String toString() {
        return new StringBuilder().append(getModel())
                .append("(").append(getCarNumber()).append(")").toString();
    }
    
    @Override
    public int compareTo(Object obj) {
        Car c=(Car) obj;
        return getModel().compareTo(c.getModel());
    }
}
////////////////////////////////////////////////////////////////////////////////