////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.model;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.*;
import java.io.*;
import java.util.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
public class Customer extends Entity implements Serializable, Comparable {
    private Long ITN;
    private String lastName;
    private String firstName;
    private String passportSeries;
    private Integer passportNumber;
    private Integer rentPeriod;
    
    private Set<Order> setOrder=new HashSet();
    
    public Customer() {}
    public Customer(Long ITN, String lastName, String firstName,
            String passportSeries, Integer passportNumber) {
        super(ITN);
        this.lastName=lastName;
        this.firstName=firstName;
        this.passportSeries=passportSeries;
        this.passportNumber=passportNumber;
    }

    public Long getITN() {
        return ITN;
    }
    public void setITN(Long ITN) {
        if(ITN==null)
            throw new IllegalArgumentException();
        this.ITN = ITN;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        if(lastName==null)
            throw new IllegalArgumentException();
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        if(firstName==null)
            throw new IllegalArgumentException();
        this.firstName = firstName;
    }

    public String getPassportSeries() {
        return passportSeries;
    }
    public void setPassportSeries(String passportSeries) {
        if(passportSeries==null)
            throw new IllegalArgumentException();
        this.passportSeries = passportSeries;
    }

    public Integer getPassportNumber() {
        return passportNumber;
    }
    public void setPassportNumber(Integer passportNumber) {
        if(passportNumber==null)
            throw new IllegalArgumentException();
        this.passportNumber = passportNumber;
    }

    public Integer getRentPeriod() {
        return rentPeriod;
    }
    public void setRentPeriod(Integer rentPeriod) {
        if(rentPeriod==null)
            throw new IllegalArgumentException();
        this.rentPeriod = rentPeriod;
    }
    
    //@OneToMany
    public Set<Order> getSetOrder() {
        return setOrder;
    }
    public void setSetOrder(Set<Order> setOrder) {
        this.setOrder = setOrder;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj==this)
            return true;
        if(obj==null)
            return false;
        if(getClass()!=obj.getClass())
            return false;
        Customer c=(Customer) obj;
        return (this.getITN()==c.getITN());
    }
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 13;
        result = PRIME * result + getITN().hashCode();
        return result;
    }
    @Override
    public String toString() {
        return new StringBuilder().append(getLastName()).append(" ").
                append(getFirstName()).append("(").append(getITN()).append(")").toString();
    }
    @Override
    public int compareTo(Object obj) {
        Customer c=(Customer) obj;
        return getLastName().compareTo(c.getLastName());
    }
}
////////////////////////////////////////////////////////////////////////////////