////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.model;
////////////////////////////////////////////////////////////////////////////////
import java.io.*;
import java.util.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class Order extends Entity implements Serializable, Comparable {
    private Long id;
    private Date firstRentalDay;
    private Date lastRentalDay;
    private String comment; 
    
    private Car car;
    
    public Order() {} 
    public Order(Long id, Date firstRentalDay, Date lastRentalDay) {
        super(id);
        this.firstRentalDay=firstRentalDay;
        this.lastRentalDay=lastRentalDay;
    }
    public Order(Long id, Date firstRentalDay, Date lastRentalDay, Car car) {
        this(id, firstRentalDay,lastRentalDay);
        this.car=car;
    }

    public Date getFirstRentalDay() {
        return firstRentalDay;
    }
    public void setFirstRentalDay(Date firstRentalDay) {
        if(firstRentalDay==null)
            throw new IllegalArgumentException();
        this.firstRentalDay = firstRentalDay;
    }

    public Date getLastRentalDay() {
        return lastRentalDay;
    }
    public void setLastRentalDay(Date lastRentalDay) {
        if(lastRentalDay==null)
            throw new IllegalArgumentException();
        this.lastRentalDay = lastRentalDay;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        if(comment==null)
            throw new IllegalArgumentException();
        this.comment = comment;
    }

    public Car getCar() {
        return car;
    }
    public void setCar(Car car) {
        if(car==null)
            throw new IllegalArgumentException();
        this.car = car;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj==this)
            return true;
        if(obj==null)
            return false;
        if(getClass()!=obj.getClass())
            return false;
        Order ord=(Order) obj;
        return (
                getId()==ord.getId() 
                );
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 13;
        result = PRIME * result + getId().hashCode();
        return result;
    }
    
    @Override
    public String toString() {
        return new StringBuilder().append(getId())
                .append(". - ").append(getCar()).append(".").toString();
    }
    
    @Override
    public int compareTo(Object obj) {
        Order ord=(Order) obj;
        return Long.compare(getId(), ord.getId());
    }
}
////////////////////////////////////////////////////////////////////////////////