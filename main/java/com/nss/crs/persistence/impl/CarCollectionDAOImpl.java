////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.persistence.impl;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.Car;
import com.nss.crs.persistence.*;
import com.nss.crs.persistence.store.CollectionStore;
import java.util.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class CarCollectionDAOImpl extends CarDAO {
    
    private List<Car> listCar;
    
    public CarCollectionDAOImpl() {
        listCar=CollectionStore.INSTANCE.getCarStore();
    }
    
    @Override
    public List<Car> findAll() {
        return listCar;
    }

    @Override
    public Car findEntityByID(Long id) {
        for(Car car:listCar)
            if(car.getId() == id)
                return car;
        return null;
    }

    @Override
    public boolean insert(Car car) {
        if(listCar.contains(car))
            return false;
        return listCar.add(car);
    }

    @Override
    public Car update(Car car) {
        for(Car c:listCar)
            if(c.getId() == car.getId()) {
                c.setId(car.getId());
                c.setCompanyManufacturer(car.getCompanyManufacturer());
                c.setModel(car.getModel());
                c.setBodyType(car.getBodyType());
                c.setCarNumber(car.getCarNumber());
                c.setDateConstruction(car.getDateConstruction());
                return c;
            }
        return null;              
    }

    @Override
    public boolean delete(Car car) {
        return listCar.remove(car);
    }

    @Override
    public Car findCarByModel(String Model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Car findCarByCompanyManufacturer(String companyManufacturer) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Car findCarByNumber(String Number) {
        throw new UnsupportedOperationException("Not supported yet.");
    }      
}
////////////////////////////////////////////////////////////////////////////////