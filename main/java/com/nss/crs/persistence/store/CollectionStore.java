////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.persistence.store;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.*;
import java.util.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public enum CollectionStore{
    INSTANCE;
    private final List<Car> listCar = new ArrayList();
    private final List<Order> listOrder = new ArrayList();
    
    private CollectionStore() {
        init();
    }
    
    private void init() {
        listCar.add(new Car(1L, "BMW", "325iM", "sedan", "AB3456BA",
                    new Date()));
        listCar.add(new Car(2L, "BMW", "525iM", "sedan", "AB3446BA",
                    new Date()));
        listCar.add(new Car(3L, "BMW", "325iM", "sedan", "AB3656BA",
                    new Date()));
        listCar.add(new Car(4L, "BMW", "750", "sedan", "AB7777BA",
                    new Date())); 
    }
    
    public List<Car> getCarStore() {
        return listCar;
    }
    
    public List<Order> getOrderStore() {
        return listOrder;
    }
}
////////////////////////////////////////////////////////////////////////////////