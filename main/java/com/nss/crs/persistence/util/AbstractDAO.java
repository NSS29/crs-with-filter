////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.persistence.util;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.Entity;
import java.io.*;
import java.util.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public abstract class AbstractDAO<K extends Serializable, T extends Entity> {
    public abstract List<T> findAll();
    public abstract T findEntityByID(K id);
    public abstract boolean insert(T entity);
    public abstract T update(T entity);
    public abstract boolean delete(T entity);    
}
////////////////////////////////////////////////////////////////////////////////