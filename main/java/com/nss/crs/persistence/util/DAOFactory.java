////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.persistence.util;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.persistence.*;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
enum CollectionType {
    COLLECTION,
    JDBC,
    HIBERNATE,
    JPA
}
public abstract class DAOFactory {
    
    private static String getTypeDAO() {
        Properties props = new Properties();
        InputStream istream = DAOFactory.class.getClassLoader().getResourceAsStream(
                "config.properties");
	try {
            props.load(istream);
	} catch (IOException ioe) {
            Logger.getLogger(DAOFactory.class.getName()).log(Level.SEVERE, null, ioe);
	}
        return props.getProperty("dao.type");
    }
    
    public static EntityDAOFactory getDAOFactory() {
        CollectionType sign=CollectionType.valueOf(getTypeDAO().toUpperCase());
        switch(sign) {
            case COLLECTION: return new CollectionDAOFactory();
            default:
                throw new EnumConstantNotPresentException(CollectionType.class, sign.name());
        }
    }
}
////////////////////////////////////////////////////////////////////////////////
