<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>	
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Edit Page</title>
    </head>
    <body>
    <center>
        <div id="mystyle" class="myform">
            <form id="form" name="form" method="POST" action="CarInfo?action=edit">
                <h1>Edit parameters</h1>
                <p>Change information</p>
                
                <label>Id
                    <span class="small">Enter car's id</span>
		</label>
                <input type="text" name="id" id="id" value="${car.getId()}" />
                
                <label>Company
                    <span class="small">Enter car's company </span>
		</label>
		<input type="text" name="company" id="company" value="${car.getCompanyManufacturer()}" />
                
                <label>Model
                    <span class="small">Enter car's model</span>
		</label>
		<input type="text" name="model" id="model" value="${car.getModel()}" />
                
                <label>Body Type
                    <span class="small">Enter car's body type</span>
		</label>
		<input type="text" name="body" id="body" value="${car.getBodyType()}" />
                
                <label>Car's Number
                    <span class="small">Enter car's number</span>
		</label>
		<input type="text" name="number" id="number" value="${car.getCarNumber()}" />
                
                <label>Date Construction
                    <span class="small">Enter car's date construction</span>
		</label>
		<input type="text" name="date" id="date" value="${car.getDateConstruction()}" />
                
                <button type="submit">Save</button>
                <button type="reset">Reset</button>
		<div class="spacer"></div>
                
            </form>
        </div>
    </center>
    </body>
</html>