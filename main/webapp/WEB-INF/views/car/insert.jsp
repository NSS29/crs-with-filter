<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>	
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Insertation Page</title>
    </head>
    <body>
    <center>
        <div id="mystyle" class="myform">
            <form id="form" name="form" method="POST" action="CarInfo?action=insert">
                <h1>Insertation</h1>
                <p>Please enter the following information</p>
                
                <label>Id
                    <span class="small">Enter car's id</span>
		</label>
                <input type="text" name="id" id="id" />
                
                <label>Company
                    <span class="small">Enter car's company </span>
		</label>
		<input type="text" name="company" id="company" />
                
                <label>Model
                    <span class="small">Enter car's model</span>
		</label>
		<input type="text" name="model" id="model" />
                
                <label>Body Type
                    <span class="small">Enter car's body type</span>
		</label>
		<input type="text" name="body" id="body" />
                
                <label>Car's Number
                    <span class="small">Enter car's number</span>
		</label>
		<input type="text" name="number" id="number" />
                
                <label>Date Construction
                    <span class="small">Enter car's date construction</span>
		</label>
		<input type="text" name="date" id="date" />
                
                <button type="submit">Save</button>
                <button type="reset">Reset</button>
		<div class="spacer"></div>
                
            </form>
        </div>
    </center>
    </body>
</html>