<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="com.nss.crs.model.*,java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <link rel="stylesheet" type="text/css" href="style.css">
    <head><title>Home Page</title></head>
    <body>
    <center>
        <div id="mystyle">
            <h1> 
                <b>Information about all  available cars.</b>  
            </h1>
            <h3><a href="CarInfo?action=insert">Insert cat to collection</a></h3>
            <strong>List of Cars</strong>
            <table border="3">
                <tbody>
                    <tr>
                        <th>ID</th>
                        <th>Company Manufacturer</th>
                        <th>Model</th>
                        <th>Body Type</th>
                        <th>Car Number</th>
                        <th>Date Construction</th>
                        <th>Order Number</th>
                        <th>Delete Record</th>
                        <th>Choose Car</th>
                    </tr>
                    <c:forEach items="${listCar}" var="car">
                        <tr>
                            <td><c:out value="${car.getId()}" /></td>
                            <td><c:out value="${car.getCompanyManufacturer()}" /></td>
                            <td><a href="CarInfo?action=edit&carId=${car.getId()}">
                                    <c:out value="${car.getModel()}" /></a>
                            </td>
                            <td><c:out value="${car.getBodyType()}" /></td>
                            <td><c:out value="${car.getCarNumber()}" /></td>
                            <td><fmt:formatDate pattern="yyyy-MMM-dd" 
                                            value="${car.getDateConstruction()}" /></td>
                            <td>
                                <c:forEach items="${car.getSetOrder()}" var="order">
                                    <c:out value="${order.getId()}, " />
                                </c:forEach>
                            </td>
                            <td><a href="CarInfo?action=delete&carId=${car.getId()}">Click</a></td>
                            <td><a href="OrderInfo?action=form&carId=${car.getId()}">Click</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </center>    
</body>
</html>