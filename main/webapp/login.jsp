<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>Login Page</title>
    </head>
    <body>
    <center>
        <div id="mystyle" class="myform">
            <form id="form" name="form" method="POST" action="Login">
                <h1>Login</h1>
                
                <p>Please enter your login information
                    <br/>New User? <a href="register.jsp">Register</a></p>
                
                <label>Login
                    <span class="small">Enter your user ID</span>
                </label>
                <input type="text" name="login" id="login" />
                
                <label>Password
                    <span class="small">Min. size 6 chars</span>
                </label>
                <input type="password" name="password" id="password" />
                
                <button type="submit">Sign-in</button>
                <div class="spacer"></div>
            </form>
        </div>
    </center>
    </body>
</html>
