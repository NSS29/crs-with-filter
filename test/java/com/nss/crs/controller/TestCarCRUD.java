////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.controller;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.controller.car.CarCRUD;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class TestCarCRUD extends Mockito {
    
    private HttpServletRequest request;  
    private HttpServletResponse response;
    private CarCRUD car;
    
    @Before
    public void setUp() throws IOException, ServletException {
        request = mock(HttpServletRequest.class);       
        response = mock(HttpServletResponse.class);
        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("company")).thenReturn("BMW");
        car = new CarCRUD();
    }
    
    public void testCarCRUD() throws ServletException, IOException {
        car.doPost(request, response); 
        //verify(request, atLeast(1)).getParameter("id"); 
        assertTrue(true);
    }
}
////////////////////////////////////////////////////////////////////////////////