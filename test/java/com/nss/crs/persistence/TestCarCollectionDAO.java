////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.persistence;
///////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.Car;
import com.nss.crs.persistence.impl.CarCollectionDAOImpl;
import java.util.*;
import org.junit.*;
import org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class TestCarCollectionDAO {
    
    private CarCollectionDAOImpl cdaoci;
    
    @Before
    public void oneTimeSetUp() {
        cdaoci=new CarCollectionDAOImpl();
        cdaoci.findAll().clear();
    }
    
    @After
    public void oneTimeTearDown() {
        cdaoci=null;
    }
    
    @Test
    public void verifyCollectionIsEmpty() {
        assertTrue(cdaoci.findAll().isEmpty());
    }
    
    @Test
    public void verifyInsertCarToCollection() {
        Car car=new Car(1L, "BMW", "325iM", "sedan", "AB3456BA",
                    new Date());
        cdaoci.insert(car);
        assertEquals(1, cdaoci.findAll().size());
    }
    
    @Test
    public void verifyUpdatetCar() {
        Car car_1=new Car(1L, "BMW", "325iM", "sedan", "AB3456BA",
                    new Date());
        Car car_2=new Car(1L, "BMW", "325iM", "universal", "AB3456BA",
                    new Date());
        cdaoci.insert(car_1);
        cdaoci.update(car_2);
        assertEquals("universal", cdaoci.findEntityByID(1L).getBodyType());
    }
    
    @Test
    public void verifyRemoveEntetyFromCollection() {
        Car car=new Car(1L, "BMW", "325iM", "sedan", "AB3456BA",
                    new Date());
        cdaoci.insert(car);
        cdaoci.delete(car);
        assertEquals(0, cdaoci.findAll().size());
    }
    
}
////////////////////////////////////////////////////////////////////////////////